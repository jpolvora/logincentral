using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Caching;
using System.Security.Principal;
using System.Web.Configuration;
using LoginCentral.Entities;
using CacheItemPriority = System.Runtime.Caching.CacheItemPriority;

namespace LoginCentral.HttpModule
{
    public static class CacheApi
    {
        private static readonly object Locker = new object();
        private static readonly int SecondsCache;
        private const string CACHE_KEY = "keepUserInCache";
        private const int DefaultCacheExpiration = 60;

        static CacheApi()
        {
            if (!WebConfigurationManager.AppSettings.AllKeys.Contains(CACHE_KEY)) return;
            if (int.TryParse(WebConfigurationManager.AppSettings[CACHE_KEY], out SecondsCache))
            {
                return;
            }

            SecondsCache = DefaultCacheExpiration;
        }

        public static User GetUser(string userName)
        {
            var cacheEntry = MemoryCache.Default.Get(userName) as UserProfileCacheEntry;
            return cacheEntry != null
                ? cacheEntry.User
                : null;
        }

        internal static void UpdateCache(string userName, User user)
        {
            lock (Locker)
            {
                var cacheEntry = MemoryCache.Default.Get(userName) as UserProfileCacheEntry;
                if (cacheEntry != null)
                {
                    MemoryCache.Default.Remove(userName);
                }
                if (user != null)
                {
                    MemoryCache.Default.Set(userName, new UserProfileCacheEntry(user), new CacheItemPolicy
                    {
                        AbsoluteExpiration = DateTimeOffset.UtcNow.AddSeconds(SecondsCache),
                        RemovedCallback = RemovedCallback,
                        //SlidingExpiration = TimeSpan.FromMinutes(5),
                        Priority = CacheItemPriority.Default
                    });
                }
            }
        }

        private static void RemovedCallback(CacheEntryRemovedArguments arguments)
        {
            Trace.TraceInformation("Usu�rio '{0}' removido do cache de autentica��o", arguments.CacheItem);
        }

        /// <summary>
        /// Ao alterar o usu�rio, limpar do cache para atualizar do banco de dados na pr�xima chamada
        /// </summary>
        /// <param name="principal"></param>
        public static void ClearUserFromCache(this IPrincipal principal)
        {
            if (principal != null && principal.Identity != null)
            {
                var userName = principal.Identity.Name;
                Trace.TraceInformation("Removendo usu�rio do cache devido a altera��es");
                UpdateCache(userName, null);
            }

        }
    }
}