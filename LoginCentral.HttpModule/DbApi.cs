﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using LoginCentral.Entities;

namespace LoginCentral.HttpModule
{
    public static class DbApi
    {
        public static User GetUser(string userName)
        {
            Trace.TraceInformation("Verificando se usuário {0} existe", userName);
            using (var ctx = new LoginCentralContext())
            {
                var user = ctx.Users
                    .Include(x => x.UserRoles)
                    .Include(x => x.UserRoles.Select(s => s.Role))
                    .FirstOrDefault(x => x.Login.Equals(userName, StringComparison.OrdinalIgnoreCase));

                if (user != null)
                {
                    CacheApi.UpdateCache(userName, user);
                }
                else
                {
                    CacheApi.UpdateCache(userName, null);
                }


                Trace.TraceInformation("Usuário {0} existe = {1}", userName, user != null);
                return user;
            }
        }
    }
}
