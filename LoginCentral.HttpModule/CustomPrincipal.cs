using System;
using System.Security.Principal;
using System.Web.Security;

namespace LoginCentral.HttpModule
{
    public class CustomPrincipal : GenericPrincipal
    {
        static string[] ExtractUserData(FormsAuthenticationTicket ticket)
        {
            var rolesId = ticket.UserData.Split(';');
            foreach (var roleId in rolesId)
            {
                //captura o nome do Role
            }

            return rolesId;
        }

        public CustomPrincipal(FormsIdentity identity)
            : base(identity, ExtractUserData(identity.Ticket))
        {
        }
    }
}