﻿using System;
using System.Linq;
using LoginCentral.Entities;

namespace LoginCentral.HttpModule
{
    //[Serializable]
    internal class UserProfileCacheEntry
    {
        public User User { get; private set; }
        
        public string[] Roles { get; private set; }

        public UserProfileCacheEntry(User user)
        {
            User = user;
            Roles = user.UserRoles.Select(s => s.Role.RoleName).ToArray();
        }

        public override string ToString()
        {
            return string.Format("{0} - {1}", User != null ? User.ToString() : "-1", Roles != null ? Roles.Count() : 0);
        }
    }
}