﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using LoginCentral.Entities;
using LoginCentral.HttpModule;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(LoginModule), "PreStart")]

namespace LoginCentral.HttpModule
{

    public class LoginModule : IHttpModule
    {

        public static void PreStart()
        {
            DynamicModuleUtility.RegisterModule(typeof(LoginModule));
        }

        public void Init(HttpApplication context)
        {
            //eventos adicionados na ordem de ocorrência
            context.AuthenticateRequest += OnAuthenticateRequest;
            context.AuthenticateRequest += OnAuthenticateRequest;
            context.PostAuthenticateRequest += OnPostAuthenticateRequest;
            context.AuthorizeRequest += OnAuthorizeRequest;
            context.EndRequest += OnEndRequest;
        }

        private static void OnAuthenticateRequest(object sender, EventArgs e)
        {
            //Aqui o FormsAuthentication faz sua mágica   


        }

        private static void OnPostAuthenticateRequest(object sender, EventArgs e)
        {
            var application = (HttpApplication)sender;

            var context = application.Context;
            var user = context.User;
            var request = context.Request;
            var response = context.Response;

            //Forms auth disabled
            if (!FormsAuthentication.IsEnabled)
                return;

            //authentication type set in web.config
            if (user == null || string.IsNullOrEmpty(user.Identity.AuthenticationType))
                return;

            //current request doesn't need authorization/authentication
            if (context.SkipAuthorization)
                return;

            //check for web.config location paths authorizing all users *
            if (IsAnonymousAccessAllowed(request))
                return;

            var formsIdentity = user.Identity as FormsIdentity;
            if (formsIdentity == null)
                return;

            if (!user.Identity.IsAuthenticated)
            {
                //se o usuário não estiver autenticado,
                //não é necessário validar no banco de dados
                return;
            }

            try
            {
                //recupera informações do usuário no banco de dados

                var usuario = GetUserFromDb(user.Identity.Name);

                if (usuario == null || !usuario.Status)
                {
                    //faz o logout se o usuário foi excluído ou desativado
                    FormsAuthentication.SignOut();

                    if (!Utilities.IsAjaxRequest(request))
                    {
                        //redireciona o usuário para a página de login (web.config)
                        FormsAuthentication.RedirectToLoginPage("appName=" + WebConfigurationManager.AppSettings["appName"]);
                    }
                    else
                    {
                        //se for uma requisição ajax, retorna o status 401 ou 403
                        response.StatusCode = 401; //403 ?
                        response.StatusDescription = "Usuário desativado!";
                    }
                    return;
                }

                //troca o generic principal
                var customPrincipal = new CustomPrincipal(formsIdentity);

                Thread.CurrentPrincipal = context.User = customPrincipal;
            }
            catch (Exception ex)
            {
                //logar exception
                Trace.TraceError("Erro: {0}", ex);
                throw;
            }
        }

        private static void OnAuthorizeRequest(object sender, EventArgs eventArgs)
        {

        }


        /// <summary>
        /// Trata o redirecionamento do FormsAuthentication
        /// </summary>
        private static void OnEndRequest(object sender, EventArgs e)
        {
            var application = (HttpApplication)sender;
            var response = application.Response;

            if (!Utilities.IsFormsAuthRedirect(response))
                return;

            var url = application.Request.Url.AbsoluteUri;
            var queryString = HttpUtility.ParseQueryString(application.Request.Url.Query);
            queryString[Utilities.GetReturnUrlVar] = HttpUtility.UrlEncode(url);
            var finalRedirectUrl = FormsAuthentication.LoginUrl + "?" + queryString;

            response.RedirectLocation = finalRedirectUrl;
        }

        static User GetUserFromDb(string login)
        {
            return CacheApi.GetUser(login) ?? DbApi.GetUser(login);
        }

        public void Dispose()
        {

        }

        public static bool IsAnonymousAccessAllowed(HttpRequest request)
        {
            // unfortunately checking if a page allows anonymous access is more complicated than you'd think(I think).
            // here we have to create a "Fake" IPrincipal that will only ever have access to 
            // pages that allow anonymous access.  That way if our fake principal has access,
            // then anonymous access is allowed

            return UrlAuthorizationModule.CheckUrlAccessForPrincipal(request.Path, AnonymousPrincipal.Instance, request.RequestType);
        }

        private class AnonymousPrincipal : IPrincipal
        {
            private static AnonymousPrincipal _Instance;
            public static AnonymousPrincipal Instance
            {
                get
                {
                    if (_Instance == null)
                        _Instance = new AnonymousPrincipal();

                    return _Instance;
                }
            }

            private AnonymousPrincipal()
            {
                _Identity = new AnonymousIdentity();
            }

            private readonly IIdentity _Identity;

            #region IPrincipal Members

            public IIdentity Identity
            {
                get { return _Identity; }
            }

            public bool IsInRole(string role)
            {
                return false;
            }

            #endregion

            private class AnonymousIdentity : IIdentity
            {
                #region IIdentity Members
                public string AuthenticationType
                {
                    get { return string.Empty; }
                }

                public bool IsAuthenticated
                {
                    get { return false; }
                }

                public string Name
                {
                    get { return string.Empty; }
                }
                #endregion
            }
        }
    }
}
