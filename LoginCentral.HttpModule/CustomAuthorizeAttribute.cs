﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;

namespace LoginCentral.HttpModule
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            Trace.TraceWarning("Unauthorized...");
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.RequestContext.HttpContext.Response.SuppressFormsAuthenticationRedirect = true;
                filterContext.RequestContext.HttpContext.Response.TrySkipIisCustomErrors = true;
            }

            if (filterContext.RequestContext.HttpContext.Request.IsAuthenticated)
            {
                //está autenticado mas não possui permissões (roles)
                //o FormsAuthentication intercepta 401, então mudamos para 403.
                filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Forbidden, "Você está autenticado mas não possui permissões para acessar a página solicitada"); //403
                return;
            }

            base.HandleUnauthorizedRequest(filterContext);
        }
    }
}
