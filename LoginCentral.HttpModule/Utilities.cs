﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using Newtonsoft.Json;

namespace LoginCentral.HttpModule
{
    public static class Utilities
    {
        public static string ToJson<T>(this T @object)
        {
            return JsonConvert.SerializeObject(@object);
        }

        public static int SetAuthCookie<T>(this HttpResponseBase responseBase, string name, bool rememberMe, T userData)
        {
            var cookie = FormsAuthentication.GetAuthCookie(name, rememberMe);
            var ticket = FormsAuthentication.Decrypt(cookie.Value);

            var newTicket = new FormsAuthenticationTicket(ticket.Version, ticket.Name, ticket.IssueDate, ticket.Expiration,
                ticket.IsPersistent, userData.ToJson(), ticket.CookiePath);
            var encTicket = FormsAuthentication.Encrypt(newTicket);
            cookie.HttpOnly = true;
            
            cookie.Value = encTicket;
            responseBase.Cookies.Remove(cookie.Name);
            responseBase.Cookies.Add(cookie);

            return encTicket.Length;
        }

        public static bool IsAjaxRequest(this HttpRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            return (request["X-Requested-With"] == "XMLHttpRequest") || ((request.Headers != null) && (request.Headers["X-Requested-With"] == "XMLHttpRequest"));
        }

        public static string GetReturnUrlVar
        {
            get
            {
                var returnUrlVar = WebConfigurationManager.AppSettings["aspnet:FormsAuthReturnUrlVar"];
                if (string.IsNullOrWhiteSpace(returnUrlVar))
                    returnUrlVar = "ReturnUrl";

                return returnUrlVar;
            }
        }

        /// <summary>
        /// Checa se ocorreu um redirecionamento iniciado pelo FormAuthenticationModule
        /// </summary>
        public static bool IsFormsAuthRedirect(this HttpResponse response)
        {
            if (response.StatusCode != 302)
                return false;

            if (string.IsNullOrEmpty(response.RedirectLocation))
                return false;

            var redirectUrl = response.RedirectLocation.ToLowerInvariant();

            var loginPage = FormsAuthentication.LoginUrl.ToLowerInvariant();

            if (!redirectUrl.StartsWith(loginPage))
                return false;

            var uri = new Uri(redirectUrl, UriKind.Absolute);

            var nvc = HttpUtility.ParseQueryString(uri.Query);

            var returnUrl = nvc[GetReturnUrlVar];

            return !string.IsNullOrWhiteSpace(returnUrl);
        }

    }
}
