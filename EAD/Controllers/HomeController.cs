﻿using System.Web.Mvc;
using LoginCentral.HttpModule;

namespace EAD.Controllers
{
    //[Authorize]
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Admin()
        {
            return View();
        }
    }
}