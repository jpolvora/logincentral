﻿using System.Web.Mvc;
using System.Web.Routing;
using LoginCentral.HttpModule;

namespace EAD
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            GlobalFilters.Filters.Add(new CustomAuthorizeAttribute());
            GlobalFilters.Filters.Add(new HandleErrorAttribute());
            //LoginCentralContext.Intialize();
        }
    }
}
