﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LoginCentral.Entities
{
    [Table("Users_Roles")]
    public class UserRole
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        [ForeignKey("RoleId")]
        public Role Role { get; set; }

        [Required]
        public long UserId { get; set; }

        [Required]
        public int RoleId { get; set; }

        public RolePermission Permission { get; set; }
    }
}