﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace LoginCentral.Entities
{
    [Table("Users")]
    public class User
    {
        [Key]
        public long Id { get; set; }

        [Required, StringLength(100)]
        public string Login { get; set; }

        [Required, StringLength(8)]
        public string Salt { get; set; }

        [Required, StringLength(44)]
        public string Hash { get; set; }

        public bool Status { get; set; }

        public ICollection<UserRole> UserRoles { get; set; }

        public override string ToString()
        {
            return string.Format("{0} - {1}", Id, Login);
        }

        public string[] GetRoles()
        {
            return UserRoles != null
                ? UserRoles.Select(s => s.Role.RoleName).ToArray()
                : Enumerable.Empty<string>().ToArray();
        }
    }
}
