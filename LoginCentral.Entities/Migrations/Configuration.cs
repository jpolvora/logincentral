using System.Data.Entity.Migrations;
using LoginCentral.HttpModule;

namespace LoginCentral.Entities.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<LoginCentralContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "LoginCentral";
        }

        protected override void Seed(Entities.LoginCentralContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //cria alguns roles default

            context.Roles.AddOrUpdate(x => x.RoleName,
                new Role() { RoleName = "Admin", Status = true },
                new Role() { RoleName = "Aluno", Status = true },
                new Role() { RoleName = "Funcionario", Status = true }
                );


            //cria um usu�rio
            var pair = SaltedHash.Instance.GetHashAndSaltString("123456");

            var user = new User() { Login = "Admin", Status = true, Hash = pair.Item1, Salt = pair.Item2 };
            context.Users.AddOrUpdate(x => x.Login, user);

            context.SaveChanges();
        }
    }
}
