﻿using System;
using System.Data.Entity;
using System.Diagnostics;
using LoginCentral.Entities.Migrations;

namespace LoginCentral.Entities
{
    public class LoginCentralContext : DbContext
    {
        static LoginCentralContext()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<LoginCentralContext, Configuration>());
        }

        public static void Intialize()
        {
            using (var ctx = new LoginCentralContext())
            {
                ctx.Database.Initialize(false);
            }
        }

        public LoginCentralContext()
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
            this.Database.Log = Log;
        }

        private static void Log(string s)
        {
            Trace.TraceInformation(s);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }

        public DbSet<UserRole> UserRoles { get; set; }
    }
}