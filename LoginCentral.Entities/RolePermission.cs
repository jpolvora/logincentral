using System;

namespace LoginCentral.Entities
{
    [Flags]
    public enum RolePermission
    {
        None = 0,
        Read = 1,
        Write = 2,
        Full = 4
    }
}