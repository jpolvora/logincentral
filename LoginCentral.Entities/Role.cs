﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LoginCentral.Entities
{
    [Table("Roles")]
    public class Role
    {
        public Role()
        {
            UserRoles = new HashSet<UserRole>();
        }

        [Key]
        public int Id { get; set; }

        public string RoleName { get; set; }

        public bool Status { get; set; }

        public ICollection<UserRole> UserRoles { get; set; }

    }
}