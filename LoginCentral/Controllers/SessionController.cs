﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using LoginCentral.Entities;
using LoginCentral.HttpModule;

namespace LoginCentral.Controllers
{
    public class SessionController : Controller
    {
        // GET: Session

        [AllowAnonymous]
        public ActionResult Index()
        {
            var roles = new List<Role>();

            if (Request.IsAuthenticated)
            {
                using (var ctx = new LoginCentralContext())
                {
                    var dbRoles = ctx.Roles.ToList();

                    roles.AddRange(dbRoles);
                }
            }
            return View(roles);
        }

        [HttpGet, AllowAnonymous]
        public ActionResult New()
        {
            if (Request.IsAuthenticated)
            {
                return Redirect(FormsAuthentication.GetRedirectUrl(User.Identity.Name, true));
            }
            return View();
        }

        [HttpPost, AllowAnonymous, ValidateAntiForgeryToken]
        public ActionResult New(string userName, string password, bool rememberMe = true)
        {
            FormsAuthentication.SignOut();

            using (var ctx = new LoginCentralContext())
            {
                var dbUser = ctx.Users.FirstOrDefault(x => x.Login.Equals(userName));

                if (dbUser == null || !dbUser.Status)
                {
                    ModelState.AddModelError("", "Usuário inexistente");
                }
                else if (SaltedHash.Instance.VerifyHashString(password, dbUser.Hash, dbUser.Salt))
                {
                    Response.SetAuthCookie(dbUser.Login, rememberMe, dbUser.UserRoles);

                    //FormsAuthentication.SetAuthCookie(dbUser.Login, true);
                    var url = HttpUtility.UrlDecode(FormsAuthentication.GetRedirectUrl(dbUser.Login, true));
                    if (string.IsNullOrEmpty(url))
                    {
                        return RedirectToAction("Index");
                    }
                    return Redirect(url);
                }

                ModelState.AddModelError("", "Senha inválida");
                return View();
            }
        }

        //[Authorize]
        public ActionResult AddRole(int? roleId)
        {
            if (!roleId.HasValue)
            {
                return RedirectToAction("Index");
            }

            using (var ctx = new LoginCentralContext())
            {
                var role = ctx.Roles.FirstOrDefault(x => x.Id == roleId.Value);
                if (role == null || !role.Status)
                {
                    return RedirectToAction("Index");
                }

                var user = ctx.Users
                    .Include(x => x.UserRoles)
                    .Include(x => x.UserRoles.Select(s => s.Role))
                    .FirstOrDefault(x => x.Login.Equals(User.Identity.Name, StringComparison.OrdinalIgnoreCase));

                if (user == null)
                {
                    return RedirectToAction("Index");
                }

                CacheApi.ClearUserFromCache(User);

                if (user.UserRoles.Any(x => x.RoleId == roleId.Value))
                {
                    //já possui a role
                    return RedirectToAction("Index");
                }

                var userrole = new UserRole()
                {
                    User = user,
                    Role = role
                };

                ctx.UserRoles.Add(userrole);
                try
                {
                    ctx.SaveChanges();
                }
                finally
                {

                }

            }

            return RedirectToAction("Index");
        }

        //[Authorize]
        public ActionResult RemoveRole(int? roleId)
        {
            if (!roleId.HasValue)
            {
                return RedirectToAction("Index");
            }

            using (var ctx = new LoginCentralContext())
            {
                var role = ctx.Roles.FirstOrDefault(x => x.Id == roleId.Value);
                if (role == null || !role.Status)
                {
                    return RedirectToAction("Index");
                }

                var user = ctx.Users
                    .Include(x => x.UserRoles)
                    .Include(x => x.UserRoles.Select(s => s.Role))
                    .FirstOrDefault(x => x.Login.Equals(User.Identity.Name, StringComparison.OrdinalIgnoreCase));

                if (user == null)
                {
                    return RedirectToAction("Index");
                }

                var existe = user.UserRoles.FirstOrDefault(x => x.RoleId == roleId.Value);
                if (existe != null)
                {
                    ctx.UserRoles.Remove(existe);
                    try
                    {
                        ctx.SaveChanges();
                    }
                    finally
                    {
                        CacheApi.ClearUserFromCache(User);
                    }
                }
            }

            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        public ActionResult Delete()
        {
            if (Request.IsAuthenticated)
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
            }
            return RedirectToAction("Index");
        }

        [CustomAuthorize(Roles = "Aluno")]
        public ActionResult Aluno()
        {
            return Content("Sou um aluno!");
        }

        [CustomAuthorize(Roles = "Funcionario")]
        public ActionResult Funcionario()
        {
            return Content("Sou um funcionário!");
        }

        [CustomAuthorize(Roles = "Admin")]
        public ActionResult Admin()
        {
            return Content("Sou um Admin");
        }
    }
}