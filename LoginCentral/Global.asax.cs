﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using LoginCentral.Entities;
using LoginCentral.HttpModule;

namespace LoginCentral
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            GlobalFilters.Filters.Add(new CustomAuthorizeAttribute());
            GlobalFilters.Filters.Add(new HandleErrorAttribute());
            LoginCentralContext.Intialize();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            //
            var application = (HttpApplication)sender;
            var server = application.Server;
            var ex = server.GetLastError();
            if (ex is ThreadAbortException)
            {
                //Esta exception é lançada quando utiliza-se Response.Redirect(url, true).
                //O correto é Response.REdirect(url, false); CompleteRequest()
                //mas em alguns casos, é necessário parar imediatamente o processamento da página,
                //lançando um threadabortexception com Response.End()
                Trace.TraceWarning("Ignoring Thread abort: " + ex.Message);
                return;
            }

            var httpException = ex as HttpException ?? new HttpException("Generic exception...", ex);

            var rootException = httpException.GetBaseException();

            //todo: use HttpContext.Current.Items !!!

            //stores exception in session for later retrieval
            if (application.Context.Handler is IRequiresSessionState ||
                application.Context.Handler is IReadOnlySessionState)
            {
                // Session exists

                application.Session["exception"] = rootException;
            }
            else if (HttpContext.Current != null)
            {
                HttpContext.Current.Items["exception"] = rootException;
            }

            Trace.TraceError("[ExceptionHelper]: {0}", rootException.Message);

            if (!Request.IsLocal)
            {
                //log or send email to developer notifiying the exception ?
                LogAction(httpException);
                server.ClearError(); //limpar o erro para exibir a página customizada
            }

            var response = application.Response;
            if (response.StatusCode == 302)
            {
                server.ClearError();
                return;
            }
            var statusCode = httpException.GetHttpCode();

            //setar o statuscode para que o IIS selecione a view correta (no web.config)
            response.StatusCode = statusCode;
            response.StatusDescription = rootException.Message; //todo: colocar uma msg melhor

            switch (statusCode)
            {
                case 401: break; //formsauthentication will handle 401
                case 403:
                    {
                        if (application.Request.IsAuthenticated && application.Response.StatusCode == 403)
                        {
                            bool isAjax = application.Request.IsAjaxRequest();
                            if (!isAjax)
                            {
                                application.Response.Write("Você está autenticado mas não possui permissões para acessar este recurso");
                            }
                        }
                        break; //IIS will handle 403
                    };
                case 404:
                    break; //IIS will handle 404
                case 500:
                    {
                        break; //IIS will handle 500
                    }
                default:
                    {
                        response.Write(rootException.ToString());
                        break;
                    }
            }
        }

        static void LogAction(HttpException exception)
        {
            //envia email
            Trace.TraceError(exception.ToString());
        }
    }
}
