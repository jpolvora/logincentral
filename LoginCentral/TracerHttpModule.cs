﻿using System;
using System.Diagnostics;
using System.Web;
using System.Web.Mvc;
using LoginCentral.HttpModule;

namespace LoginCentral
{
    public class TracerHttpModule : IHttpModule
    {

        public void Init(HttpApplication on)
        {
            //http://msdn.microsoft.com/en-us/library/bb470252(v=vs.100).aspx

            on.BeginRequest += (sender, args) => OnBeginRequest(on);
            on.EndRequest += (sender, args) => OnEndRequest(on);
            on.AuthenticateRequest += (sender, args) => TraceNotification(on, "AuthenticateRequest");
            on.PostAuthenticateRequest += (sender, args) => TraceNotification(on, "PostAuthenticateRequest");
            on.AuthorizeRequest += (sender, args) => TraceNotification(on, "AuthorizeRequest");
            on.PostAuthorizeRequest += (sender, args) => TraceNotification(on, "PostAuthorizeRequest");
            on.ResolveRequestCache += (sender, args) => TraceNotification(on, "ResolveRequestCache");

            //MVC Routing module remaps the handler here.
            on.PostResolveRequestCache += (sender, args) => TraceNotification(on, "PostResolveRequestCache");
            //only iis7
            on.MapRequestHandler += (sender, args) => TraceNotification(on, "MapRequestHandler");

            //An appropriate handler is selected based on the file-name extension of the requested resource. 
            //The handler can be a native-code module such as the IIS 7.0 StaticFileModule or a managed-code module
            //such as the PageHandlerFactory class (which handles .aspx files). 
            on.PostMapRequestHandler += (sender, args) => TraceNotification(on, "PostMapRequestHandler");
            on.AcquireRequestState += (sender, args) => TraceNotification(on, "AcquireRequestState");
            on.PostAcquireRequestState += (sender, args) => TraceNotification(on, "PostAcquireRequestState");
            on.PreRequestHandlerExecute += (sender, args) => TraceNotification(on, "PreRequestHandlerExecute");
            //after Call the ProcessRequest of IHttpHandler
            on.PostRequestHandlerExecute += (sender, args) => TraceNotification(on, "PostRequestHandlerExecute");

            on.ReleaseRequestState += (sender, args) => TraceNotification(on, "ReleaseRequestState");
            //Perform response filtering if the Filter property is defined.
            on.PostReleaseRequestState += (sender, args) => TraceNotification(on, "PostReleaseRequestState");
            on.UpdateRequestCache += (sender, args) => TraceNotification(on, "UpdateRequestCache");

            on.PostUpdateRequestCache += (sender, args) => TraceNotification(on, "PostUpdateRequestCache");
            //The MapRequestHandler, LogRequest, and PostLogRequest events are supported only if the application 
            //is running in Integrated mode in IIS 7.0 and with the .NET Framework 3.0 or later.
            on.LogRequest += (sender, args) => TraceNotification(on, "LogRequest"); //iis7
            on.PostLogRequest += (sender, args) => TraceNotification(on, "PostLogRequest"); //iis7
            on.PreSendRequestHeaders += (sender, args) => TraceNotification(on, "PreSendRequestHeaders");
            on.PreSendRequestContent += (sender, args) => TraceNotification(on, "PreSendRequestContent");

        }

        private static void OnBeginRequest(HttpApplication application)
        {
            var context = application.Context;

            bool isAjax = context.Request.IsAjaxRequest();

            if (isAjax)
            {
                context.Response.SuppressFormsAuthenticationRedirect = true;
            }

            //URL REWRITE (not context.RewritePath)
            if (context.Items.Contains("IIS_WasUrlRewritten") || context.Items.Contains("HTTP_X_ORIGINAL_URL"))
            {
                Trace.TraceInformation("[TracerHttpModule]:Url was rewriten from '{0}' to '{1}'",
                    context.Request.ServerVariables["HTTP_X_ORIGINAL_URL"],
                    context.Request.ServerVariables["SCRIPT_NAME"]);
            }

            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]
                ?? context.Request.ServerVariables["REMOTE_ADDR"];

            var rid = context.GetHashCode();

            Trace.TraceInformation("[BeginRequest]:[{0}] {1} {2} {3} {4} from {5}", rid, context.Request.HttpMethod,
                context.Request.RawUrl, isAjax ? "Ajax: True" : "", context.Request.UrlReferrer, ipAddress);
        }

        private static void TraceNotification(HttpApplication application, string eventName)
        {
            var context = application.Context;
            var isPost = context.IsPostNotification;
            var postString = isPost ? "[ISPOST], " : "";

            var rid = context.GetHashCode();
            var user = application.User != null ? application.User.Identity.Name : "-";

            Trace.TraceInformation("[TracerHttpModule]:{0}{1}, rid: [{2}], handler: [{3}], user: {4}, status: {5}",
                postString, eventName, rid, application.Context.CurrentHandler,
                user, application.Context.Response.StatusCode);


            switch (context.CurrentNotification)
            {
                case RequestNotification.PreExecuteRequestHandler:
                    {
                        //will call ProcessRequest of IHttpHandler

                        var mvcHandler = context.Handler as MvcHandler;
                        if (mvcHandler != null)
                        {
                            var controller = mvcHandler.RequestContext.RouteData.GetRequiredString("controller");
                            var action = mvcHandler.RequestContext.RouteData.GetRequiredString("action");
                            var area = mvcHandler.RequestContext.RouteData.DataTokens["area"];

                            TraceToBuffer(
                                "Entering MVC Pipeline. Area: '{0}', Controller: '{1}', Action: '{2}'", area,
                                controller, action);
                        }
                        else
                        {
                            TraceToBuffer("[TracerHttpModule]:Executing ProcessRequest of Handler {0}", context.CurrentHandler);
                        }
                    }
                    break;
                case RequestNotification.ReleaseRequestState:
                    {
                        TraceToBuffer("[TracerHttpModule]:Response Filter ({0}): {1}", postString, context.Response.Filter);
                        break;
                    }
            }
        }

        private static void OnEndRequest(HttpApplication application)
        {
            var context = application.Context;

            var rid = context.GetHashCode();
            var now = DateTime.Now;
            var elapsed = (now - HttpContext.Current.Timestamp);

            var msg = string.Format("[EndRequest]:[{0}], Content-Type: {1}, Status: {2}, Render: {3} ms, url: {4}",
                rid, context.Response.ContentType, context.Response.StatusCode, elapsed.ToString("g"),
                context.Request.Url);
            Trace.TraceInformation(msg);
        }

        static void TraceToBuffer(string str, params object[] args)
        {
            if (args.Length == 0)
            {
                Trace.TraceInformation(str);
            }
            else
            {
                Trace.TraceInformation(str, args);
            }

        }

        public void Dispose()
        {
        }
    }
}
