﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;
using LoginCentral.HttpModule;

namespace Portal.Controllers
{
    //[Authorize]
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [CustomAuthorize(Roles = "Aluno")]
        public ActionResult Aluno()
        {
            return View();
        }
    }
}